package main

import (
	_ "net/http/pprof"
	log "github.com/sirupsen/logrus"
	//"flag"
	//"org.bitbucket/dmgcodevil/parapet/core/sandbox"
	"org.bitbucket/dmgcodevil/parapet/go-parapet"
	"os"
	"flag"
	"sync"
	"io"
	"org.bitbucket/dmgcodevil/parapet/sandbox"
)

type LogWriter struct {
	console io.Writer
	f io.Writer
}

func (w LogWriter)Write(p []byte) (n int, err error) {
	w.console.Write(p)
	return w.f.Write(p)
}

func init() {
	file, _ := os.OpenFile("parapet.log", os.O_CREATE|os.O_WRONLY, 0666)
	writer := LogWriter{os.Stdout, file}

	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(writer)
	log.SetLevel(log.DebugLevel)
}

func main() {
	sandbox.DemoChannel()
}

func demoRaft(){
	var wg sync.WaitGroup
	wg.Add(1)
	srvAddress := flag.String("address", "localhost:8087", "server address")
	replica := flag.String("replica", "", "replica")
	flag.Parse()


	srv, _ := parapet.NewServer(*srvAddress, parapet.Servers(*replica))
	srv.Bootstrap()
	wg.Wait()
}