package sandbox

import (
	"org.bitbucket/dmgcodevil/parapet/go-parapet"
	log "github.com/sirupsen/logrus"
	"io"
)

func DemoChannel()  {
	srv, err := parapet.NewServer("tcp://localhost:2222")
	if(err != nil) {
		log.Fatalf("failed to create server: %s", err.Error())
	}
	srv.Subscribe("ping", func(msg *parapet.Message, w io.Writer){
		w.Write([]byte("pong"))
	})
	go srv.Bootstrap()

	ch1, err := parapet.CreateNetChannel("ch1", "tcp://localhost:2222")
	if(err != nil) {
		log.Fatalf("failed to create channel: %s", err.Error())
	}
	ch1.Subscribe("", func(msg *parapet.Message) {
		log.Infof("common sub: %s", msg.String())
	})
	response, err := ch1.Request("ping", []byte("ping server"))
	if(err != nil) {
		log.Fatalf("failed to receive response: %s", err.Error())
	}
	log.Infof("response: %s", string(response))
}
