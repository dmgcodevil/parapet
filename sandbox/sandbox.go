package sandbox

import (
	"github.com/nats-io/go-nats"
	"time"
	"fmt"
)

func TestNats()  {
	nc, err := nats.Connect(nats.DefaultURL)
	if(err != nil) {
		println(err.Error())
		return
	}

	// Replies
	nc.Subscribe("help", func(m *nats.Msg) {
		fmt.Printf("send reply to '%s'\n", m.Reply)
		nc.Publish(m.Reply, []byte("I can help!"))
	})

	nc.Subscribe("", func(m *nats.Msg) {
		fmt.Printf("empty sub received: '%s'\n", string(m.Data))
	})


	// Requests
	//nc.PublishRequest()
	msg, err := nc.Request("help", []byte("help me"), 5000 * time.Millisecond)
	if(err!=nil) {
		println(err.Error())
		return
	}

	fmt.Printf("sync reply: '%s'\n", string(msg.Data))

	//nc.PublishRequest("help", []byte("hi from pub"))


	time.Sleep(5 * time.Second)

	// Close connection
	nc.Close();
}
