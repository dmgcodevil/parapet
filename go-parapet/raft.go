package parapet

const (
	Follower = iota
	Candidate
	Leader
)

type Log struct {
	entries []LogEntry
}

type LogEntry struct {
	term int
	data []byte
}

type RequestVote struct {
	//candidate’s term
	Term         int `json:"term"`

	//candidate requesting vote
	CandidateId  string `json:"candidateId"`

	//index of candidate’s last log entry
	LastLogIndex int `json:"lastLogIndex"`

	// term of candidate’s last log entry
	LastLogTerm  int `json:"lastLogTerm"`
}

type RequestVoteResult struct {
	//currentTerm, for candidate to update itself
	Term        int `json:"term"`

	//true means candidate received vote
	VoteGranted bool `json:"voteGranted"`
}

type AppendEntries struct {
	Term     int `json:"term"`
	LeaderId string `json:"leaderId"`
	entries  []LogEntry
}

type AppendEntriesResponse struct {
	// currentTerm, for leader to update itself
	Term    int `json:"term"`

	// true if follower contained entry matching
	//prevLogIndex and prevLogTerm
	Success bool `json:"success"`
}

type Status struct {
	// current state of the given server
	state        int

	//latest term server has seen (initialized to 0
	//on first boot, increases monotonically)
	currentTerm int

	//candidateId that received vote in current
	//term (or null if none)
	votedFor    string

	//log entries; each entry contains command
	//for state machine, and term when entry
	//was received by leader (first index is 1)
	log         Log

	//index of highest log entry known to be
	//committed (initialized to 0, increases
	//monotonically)
	commitIndex int

	//index of highest log entry applied to state
	//machine (initialized to 0, increases
	//monotonically)
	lastApplied int

	//for each server, index of the next log entry
	//to send to that server (initialized to leader
	//last log index + 1)
	nextIndex   [] int

	//for each server, index of highest log entry
	//known to be replicated on server
	//(initialized to 0, increases monotonically)
	matchIndex  [] int
}

func StateStrconv(state int) string {
	switch state {
	case Follower:
		return "Follower"
	case Candidate:
		return "Candidate"
	case Leader:
		return "Leader"
	default:
		return "Unknown"
	}
}
