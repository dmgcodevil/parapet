package parapet

import (
	"net/url"
	"time"
	"errors"
	"net"
	"bufio"
	"fmt"
	"log"
	"github.com/nu7hatch/gouuid"
	"sync"
)

const (
	DefaultTimeout = 2 * time.Second

	// The size of the bufio reader/writer on top of the socket.
	defaultBufSize = 32768

	subInMsgChannelSize = 100
)

const (
	_CRLF_ = '\n'
	_EMPTY_ = ""
	_SPC_ = " "
	_PUB_ = "PUB"
	_MSG_ = "MSG"
	_MSG_ACK_ = "MSG_ACK"
	_WILDCARD_ = "*"
)

// Errors
var (
	ErrTimeout = errors.New("timeout")
)

type ChannelConf struct {

}

type subscriptions map[string][]*Subscription

type Channel struct {
	id        string // todo rename to ssid and use UUID instead of 'name'
	chanMutex *sync.Mutex
	conn      net.Conn
	bw        *bufio.Writer
	br        *bufio.Reader
	os        OutStream
	is        InStream
	subs      subscriptions
	subsMutex *sync.Mutex
}

func CreateNetChannel(name, rawUrl string) (*Channel, error) {
	addr, err := url.Parse(rawUrl)
	if (err != nil) {
		return nil, err
	}

	conn, err := net.Dial(addr.Scheme, addr.Host)
	if (err != nil) {
		return nil, err
	}
	ch := NewChannel(name, conn)
	return ch, nil
}

// todo add options parameter
func NewChannel(name string, conn net.Conn) *Channel {
	bw := bufio.NewWriterSize(conn, defaultBufSize)
	br := bufio.NewReader(conn)
	inStream := NewFrameMessageInStream(br)
	outStream := NewFrameMessageOutStream(bw)
	subs := make(subscriptions)
	ch := &Channel{
		id:                name,
		chanMutex:         &sync.Mutex{},
		conn:              conn,
		bw:                bw,
		br:                br,
		os:                outStream,
		is:                inStream,
		subs:              subs,
		subsMutex:         &sync.Mutex{},
	}
	go ch.readLoop()
	return ch
}

func (ch *Channel) readLoop() {
	for {
		var msg Message
		if err := ch.is.Read(&msg); err == nil {
			ch.processMsg(&msg)
		} else {
			log.Printf("failed to read from channel[%s], error='%s'. closing channel", ch.id, err.Error())
			ch.Close()
			ch.subsMutex.Lock()
			for r, subs := range ch.subs {
				log.Printf("closing subsribtions for subject '%s'", r)
				for _, s := range subs {
					s.closeSub()
				}
			}
			ch.subsMutex.Unlock()
			return
		}

	}
}

func (ch *Channel) processMsg(msg *Message) {
	switch msg.Cmd {
	case _MSG_ACK_:
		// todo remove message from pending queue
		fmt.Printf("channel[%s] received ack msg[%d]for message[%s]\n", ch.id, msg.Clock, msg.Data)
	default:
		{
			//fmt.Printf("channel[%s] received message[clock=%d, subj=%s, payload=%s]\n", ch.id, msg.Clock, msg.Subject, string(msg.Data))
			subj := msg.Subject
			ch.subsMutex.Lock()
			for subjFilter, subs := range ch.subs {
				// todo: improve filtering to support dot delimited subjects like "one.*.three"
				if (_EMPTY_ == subjFilter || _WILDCARD_ == subjFilter || subj == subjFilter ) {
					for _, sub := range subs {
						if (sub.Closed()) {
							// todo: remove subscription once it's closed or unsubsribed [not implemented yet]
							continue
						}

						if (sub.subType == AsyncSub) {
							sub.inQueue.Add(msg)
						} else {
							sub.inMsgChan <- msg
						}
					}

				}
			}
			ch.subsMutex.Unlock()
		}
	}
}

func (ch *Channel) Request(subj string, data []byte) ([]byte, error) {
	// todo replace with channel ssid
	inUUID, err := uuid.NewV4()
	if (err != nil) {
		return nil, err
	}
	inbox := inUUID.String()
	sub, _ := ch.SubscribeSync(inbox)
	err = ch.Publish(subj, inbox, data)
	if (err != nil) {
		return nil, err
	}
	msg, err := sub.NextMessage()
	if (err != nil) {
		return nil, err
	}
	return msg.Data, nil
}

func (ch *Channel) subscribe(subj string, handler MessageHandler, msgChan chan *Message) *Subscription {
	ch.subsMutex.Lock()
	sub := NewSubscription(subj, handler, msgChan)
	subs := ch.subs[subj]
	if(subs == nil) {
		subs = make([]*Subscription, 0, 10)
	}
	ch.subs[subj] = append(subs, sub)
	ch.subsMutex.Unlock()
	return sub
}

func (ch *Channel) Publish(subj, reply string, data []byte) error {
	msg := NewMessage(_PUB_, subj, reply, data)
	return ch.publish(subj, reply, msg)
}

func (ch *Channel) publish(subj, reply string, msg *Message) error {
	if (ch.Closed()) {
		return errors.New("channel is closed")
	}
	err := ch.os.Write(msg)
	if (err != nil) {
		return err
	}
	return nil
}

func (ch *Channel) Subscribe(subj string, handler MessageHandler) (*Subscription, error) {
	return ch.subscribe(subj, handler, nil), nil
}

func (ch *Channel) SubscribeSync(subj string) (*Subscription, error) {
	return ch.subscribe(subj, nil, make(chan *Message, subInMsgChannelSize)), nil
}

func (ch *Channel) Receive(subj string, timeout time.Duration) (m *Message, err error) {
	sub, err := ch.SubscribeSync(subj)
	if (err != nil) {
		return
	}
	m, err = sub.NextMessage()
	return
}

func (ch *Channel)Close() {
	ch.chanMutex.Lock()
	if (ch.conn != nil) {
		ch.conn.Close()
		ch.conn = nil
	}
	ch.chanMutex.Unlock()
}

func (ch *Channel)Closed() bool {
	ch.chanMutex.Lock()
	closed := false
	if (ch.conn == nil) {
		closed = true
	}
	ch.chanMutex.Unlock()
	return closed
}