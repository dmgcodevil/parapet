package parapet

import (
	"net"
	"net/url"
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/nu7hatch/gouuid"
	"time"
)

// Option is a function on the options for a connection.
type Option func(*Options) error

// Options can be used to create a customized connection.
type Options struct {
	serversAddresses string
}

var DefaultOptions = Options{
	// by default server started in single node
	serversAddresses : "",
}

type Server struct {
	id           string
	url          *url.URL
	opts         Options
	listener     *net.TCPListener
	handlers     map[string]MessageHandlerWithReply
}

func NewServer(rawUrl string, options ...Option) (*Server, error) {
	serverUrl, err := url.Parse(rawUrl)
	if(err != nil) {
		return nil, err
	}
	tcpAddr, err := net.ResolveTCPAddr("tcp", serverUrl.Host)
	if(err != nil) {
		return nil, err
	}
	// todo move to connect method
	listener, err := net.ListenTCP("tcp", tcpAddr)
	if(err != nil) {
		return nil, err
	}
	opts := DefaultOptions
	// apply options
	for _, opt := range options {
		if err := opt(&opts); err != nil {
			return nil, err
		}
	}
	srv := &Server{
		id:          genServerSSID(),
		url :        serverUrl,
		opts:        opts,
		listener:    listener,
		handlers :   make(map[string]MessageHandlerWithReply),
	}
	srv.registerSystemHandlers()
	return srv, nil
}

func genServerSSID() string  {
	u, err := uuid.NewV4()
	if(err != nil) {
		log.Fatalf("failed to generate server ssid: %", err.Error())
	}
	return u.String()
}

func (srv *Server)registerSystemHandlers() {
	// todo
}

func Servers(addresses string) Option {
	return func(o *Options) error {
		o.serversAddresses = addresses
		return nil
	}
}

func (srv *Server)Subscribe(subj string, handler MessageHandlerWithReply) {
	srv.handlers[subj] = handler
}

func logger(srv *Server) *log.Entry {
	return log.WithFields(log.Fields{
		"ssid": srv.id,
	})
}

func (srv *Server) Bootstrap() {
	logger(srv).Info("server has been started")
	for {
		conn, err := srv.listener.Accept()
		if err != nil {
			fmt.Printf("server error: %s", err)
			continue
		}

		go handleClient(srv, conn)
	}
}

func handleClient(srv *Server, conn net.Conn) {
	clientChan := NewChannel("sever channel[" + conn.RemoteAddr().String() + "]", conn)

	for {
		msg, err := clientChan.Receive("", time.Minute)
		if (err != nil) {
			log.Fatal(err)
		}

		switch msg.Cmd {
		case _PUB_:
			ackMsg := createAckMsg(msg)
			clientChan.os.Write(ackMsg) // todo we should not use channel streams outside channel
			for subj, handler := range srv.handlers {
				if (subj == "" || subj == msg.Subject) {
					rw := &ReplyWriter{msg.Reply, clientChan}
					go handler(msg, rw)
				}
			}
		}
	}
}

type ReplyWriter struct {
	reply string
	ch    *Channel
}

func (rw *ReplyWriter)Write(p []byte) (n int, err error) {
	n = 0
	err = rw.ch.os.Write(NewMessage(_MSG_, rw.reply, "", p))
	if (err != nil) {
		return
	}
	n = len(p)
	return
}

func createAckMsg(inMsg *Message) *Message {
	return NewMessage(_MSG_ACK_, inMsg.Reply, "", []byte(fmt.Sprintf("clock=%d", inMsg.Clock)));
}

