package parapet

import (
	"sync"
	"errors"
	"log"
	"sync/atomic"
)

type SubType int

const (
	AsyncSub = SubType(iota)
	SyncSub
)

type Subscription struct {
	subMutex  *sync.Mutex
	subj      string
	handler   MessageHandler
	inMsgChan chan *Message
	subType   SubType

	inQueue   *RingBuffer

	// stats
	delivered uint64

	closeCh   chan bool
	closed    bool
}

func (sub *Subscription)DeliveredCount() uint64 {
	deliveredFinal := atomic.LoadUint64(&sub.delivered)
	return deliveredFinal;
}

func NewSubscription(subj string, handler MessageHandler, ch chan *Message) *Subscription {
	sub := &Subscription{
		subMutex:        &sync.Mutex{},
		subj:            subj,
		handler:         handler,
		inMsgChan:       ch,
		closeCh:         make(chan bool),
	}
	if (handler != nil) {
		sub.subType = AsyncSub
		sub.inQueue, _ = NewRingBuffer(100)
		go sub.waitForMessages()
	} else {
		sub.subType = SyncSub
	}
	return sub
}

func (sub *Subscription) closeSub() {
	sub.subMutex.Lock()
	sub.closed = true
	sub.subMutex.Unlock()
	sub.closeCh <- true
}

func (sub *Subscription) waitForMessages() {
	for {
		select {
		case <-sub.closeCh:
			log.Println("channel has been closed. closing subscribtion")
			return
		case el := <-sub.inQueue.RemoveChan():
			if msg, ok := el.(*Message); !ok {
				log.Fatalf("wrong message type: %T", msg)
			} else {
				sub.handler(msg)
				atomic.AddUint64(&sub.delivered, 1)
			}
		}
	}
}

func (sub *Subscription) NextMessage() (*Message, error) {
	if (sub.subType != SyncSub) {
		return nil, errors.New("it's prohibited to use NextMessage() for non sync subscriptions")
	}
	select {
	case <-sub.closeCh:
		return nil, errors.New("channel is closed")
	case msg := <-sub.inMsgChan:
		atomic.AddUint64(&sub.delivered, 1)
		return msg, nil
	}
}

func (sub *Subscription)Closed() bool {
	sub.subMutex.Lock()
	closed := sub.closed
	sub.subMutex.Unlock()
	return closed
}