package parapet

import "sync"

var SeqGen = New()

type SequenceGen struct {
	secNum uint64
	mutex  *sync.Mutex
}

func New() *SequenceGen {
	return &SequenceGen{0, &sync.Mutex{}}
}

func (gen *SequenceGen)Next() uint64 {
	var next uint64
	gen.mutex.Lock()
	gen.secNum++
	next = gen.secNum
	gen.mutex.Unlock()
	return next
}
