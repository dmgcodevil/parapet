package parapet

import (
	"encoding/json"
	"bufio"
	"encoding/binary"
	"io"
	"bytes"
	"sync"
)

// Writes the given value pointed to by v to the stream.
type OutStream interface {
	Write(v interface{}) error
}

// Reads the next value from the input stream and stores it in the value pointed to by v.
type InStream interface {
	Read(v interface{}) error
}

type FrameMessageInStream struct {
	mu 	*sync.Mutex
	reader *bufio.Reader
}
type FrameMessageOutStream struct {
	mu 	*sync.Mutex
	writer *bufio.Writer
}

func NewFrameMessageInStream(reader *bufio.Reader) InStream {
	return &FrameMessageInStream{&sync.Mutex{}, reader}
}

func NewFrameMessageOutStream(writer *bufio.Writer) OutStream {
	return &FrameMessageOutStream{&sync.Mutex{}, writer}
}

func (stream *FrameMessageInStream)Read(v interface{}) error {
	stream.mu.Lock()
	defer stream.mu.Unlock()
	headerBuf := make([]byte, 4)
	_, err := io.ReadFull(stream.reader, headerBuf)
	if (err != nil) {
		return err
	}

	size := binary.BigEndian.Uint32(headerBuf)

	msgBuf := make([]byte, size)

	_, err = io.ReadFull(stream.reader, msgBuf)

	if (err != nil) {
		return err
	}

	if err := json.Unmarshal(msgBuf, v); err != nil {
		return err
	}

	return nil
}

const (
	headerSizeInBytes = 4
)

// todo create high-level abstrations for Unmarshal/Marshal ab
func Unmarshal(data []byte, v interface{}) error {
	if err := json.Unmarshal(data, v); err != nil {
		return err
	}
	return nil
}
func Marshal(v interface{}) ([]byte, error) {
	data, err := json.Marshal(v)
	if (err != nil) {
		return nil, err
	}
	return data, nil
}

func (stream *FrameMessageOutStream)Write(v interface{}) error {
	stream.mu.Lock()
	defer stream.mu.Unlock()
	msgBuf, err := json.Marshal(v)
	if (err != nil) {
		return err
	}
	msgSize := uint32(len(msgBuf))
	buf := bytes.NewBuffer(make([]byte, 0, headerSizeInBytes + msgSize))
	writeSize(msgSize, buf)
	buf.Write(msgBuf)
	stream.writer.Write(buf.Bytes())
	stream.writer.Flush()
	return nil
}

func writeSize(msgSize uint32, buf *bytes.Buffer) {
	// using new byte slice to write uint32 number is a bit space consuming
	// but much cleaner then allocating new space in buffer and do slice copy
	msgSizeBuf := make([]byte, 4, 4)
	binary.BigEndian.PutUint32(msgSizeBuf, msgSize)
	buf.Write(msgSizeBuf)
}


type JsonMessageInStream struct {
	dec *json.Decoder
}

type JsonMessageOutStream struct {
	enc *json.Encoder
}

func NewJsonMessageInStream(reader *bufio.Reader) InStream {
	return &JsonMessageInStream{json.NewDecoder(reader)}
}

func NewJsonMessageOutStream(writer *bufio.Writer) OutStream {
	return &JsonMessageOutStream{json.NewEncoder(writer)}
}

func (stream *JsonMessageOutStream)Write(v interface{}) error {
	return stream.enc.Encode(v)
}

func (stream *JsonMessageInStream)Read(v interface{}) error {
	return stream.dec.Decode(v)
}