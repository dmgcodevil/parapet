package parapet

import (
	"errors"
	"sync"
)

type RingBuffer struct {
	cap      int
	elements []interface{}
	cond     *sync.Cond
	// The write pointer, represented as an offset into the array
	offset   int
	// The number of unconsumed elements, used to determine read pointer position by backing up
	// that many positions before the read position
	size     int
}

func NewRingBuffer(maxCap int) (*RingBuffer, error) {
	if (maxCap <= 0) {
		return nil, errors.New("RingBuffer capacity must be positive and greater than '0'")
	}
	return &RingBuffer{
		cap :           maxCap,
		elements        :make([]interface{}, maxCap),
		cond :          &sync.Cond{L: &sync.Mutex{}},
		offset:         0,
		size:           0,
	}, nil
}

func (ringBuf RingBuffer) Capacity() int {
	return ringBuf.cap
}

func (ringBuf *RingBuffer) Size() int {
	ringBuf.lock()
	size := ringBuf.size
	ringBuf.unlock()
	return size
}

func (ringBuf *RingBuffer) Add(element interface{}) {
	ringBuf.lock()
	for ringBuf.size == ringBuf.cap {
		ringBuf.cond.Wait()
	}
	// use bit mask to avoid overflow
	ringBuf.elements[ringBuf.offset] = element
	ringBuf.offset = (ringBuf.offset + 1) % ringBuf.cap;
	ringBuf.size++;
	ringBuf.unlock()
	ringBuf.cond.Signal()

}

func (ringBuf *RingBuffer)lock() {
	ringBuf.cond.L.Lock()
}

func (ringBuf *RingBuffer) unlock() {
	ringBuf.cond.L.Unlock()
}

func (ringBuf *RingBuffer) Peek() interface{} {
	ringBuf.lock()
	for ringBuf.size == 0 {
		ringBuf.cond.Wait()
	}
	distance := ringBuf.cap - ringBuf.size
	readPos := (ringBuf.offset + distance) % ringBuf.cap
	res := ringBuf.elements[readPos]
	ringBuf.unlock()
	return res;
}

func (ringBuf *RingBuffer) Remove() interface{} {
	res := ringBuf.Peek()
	ringBuf.lock()
	ringBuf.size --
	ringBuf.unlock()
	ringBuf.cond.Signal()
	return res
}

func (ringBuf *RingBuffer) RemoveChan() <- chan interface{} {
	outCh := make(chan interface{})
	go func() {
		outCh <- ringBuf.Remove()
	}()
	return outCh
}

