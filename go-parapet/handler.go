package parapet

import "io"

type MessageHandler func(msg *Message)

// fixme: we should use the same handle for peer
type MessageHandlerWithReply func(msg *Message, w io.Writer)