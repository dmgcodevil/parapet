package util

import (
	"strconv"
	"time"
)

func Timestamp() (uint64, error) {
	i, err := strconv.ParseInt(time.Now().Format("20060102150405"), 10, 64)
	return uint64(i), err
}
