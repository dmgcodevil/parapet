package parapet

import (
	"fmt"
)

type Message struct {
	Clock   uint64  `json:"clock"`
	Cmd     string  `json:"cmd"`
	Subject string  `json:"sub"`
	Reply   string  `json:"rep"`
	Data    []byte  `json:"data"`
}

func NewMessage(cmd string, sub string, repl string, data []byte) *Message {
	return &Message{SeqGen.Next(), cmd, sub, repl, data}
}

func (m Message) String() string {
	return fmt.Sprintf("[%d, %s, %s, %s, %s]", m.Clock, m.Cmd, m.Subject, m.Reply, string(m.Data))
}